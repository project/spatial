<?php

/**
 * @file
 * TODO: Rework module description
 */

define('SPATIAL_FILE_GROUPING_COLLECTION', 0);
define('SPATIAL_FILE_GROUPING_MULTIPLE', 1);
define('SPATIAL_FILE_GROUPING_BYTYPE', 2);

/**
 * Implements hook_field_widget_info().
 */
function spatial_file_field_widget_info() {
  return array(
    'spatial_file' => array(
      'label'       => t('Spatial file'),
      'description' => t('TODO: Change Descr - Display a list of shapefiles as a select list.'),
      'field types' => array('geofield'),
      'settings'    => array(
        'associated_filefield' => array(),
        'multiple_values' => SPATIAL_FILE_GROUPING_COLLECTION,
        'allowed_types' => array('line', 'point', 'polygon'),
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */
function spatial_file_field_widget_settings_form($field, $instance) {
  module_load_include('inc', 'spatial_file', 'spatial_file.widget');
  return _spatial_file_widget_settings_form($field, $instance);
}

/**
 * Implements hook_field_widget_form().
 */
function spatial_file_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  module_load_include('inc', 'spatial_file', 'spatial_file.widget');
  return _spatial_file_widget_form($form, $form_state, $field, $instance, $langcode, $items, $delta, $element);
}

/**
 * The #value_callback for the file_generic field element.
 */
function spatial_file_field_widget_value($element, $input = FALSE, $form_state) {
  module_load_include('inc', 'spatial_file', 'spatial_file.widget');
  return _spatial_file_widget_value($element, $input, $form_state);
}

/**
 * Implements hook_field_widget_error().
 */
// TODO: Can probably ditch this
function spatial_file_field_widget_error($element, $error, $form, &$form_state) {
//  form_error($element['fid'], 'yo: '.$error['message']);
}


/**
 * This function takes an array of spatial files (currently ESRI Shapefile and KML files are supported)
 *   and processes them into geofield value arrays
 *
 *   TODO: Add a parameter to include / exclude types
 */
function spatial_file_to_geofield($spatial_files,
                                    $group_multiple_values = SPATIAL_FILE_GROUPING_COLLECTION,
                                    $separate_files = 0
                                ) {
  $spatial_features = array();

  foreach ($spatial_files as $spatial_file) {
    $file_uri = '';

    if (is_array($spatial_file)) {
      if ($file = file_load($spatial_file['fid'])) {
        $file_uri = $file->uri;
      }
      else {
        // TODO: add a watchdog entry
        continue;
      }
    }
    else {
      // TODO: Add processing for fid ints and uri strings
      continue;
    }

    if ($file_uri) {
      if ($file_spatial_features = spatial_file_get_wkt_features($file_uri)) {
        if ($separate_files) {
          $spatial_features[] = $file_spatial_features;
        }
        else {
          $spatial_features = array_merge($spatial_features, $file_spatial_features);
        }
      }
    }
  }

  if ($spatial_features) {
    if ($separate_files) {
      $geofield_values = array();
      foreach ($spatial_features as $file_features) {
        $geofield_values[] = spatial_file_parse_wkt_features($file_features, $group_multiple_values);
      }
      return $geofield_values;
    }
    else {
      return spatial_file_parse_wkt_features($spatial_features, $group_multiple_values);
    }
  }
}

function spatial_file_get_wkt_features($uri, $enforce_extension = NULL) {
  // TODO:
  $uri_chunks = explode('.', $uri);

  if (isset($uri_chunks[1]) || !is_null($enforce_extension)) {

    $file_extension = !is_null($enforce_extension) ? $enforce_extension : array_pop($uri_chunks);
    // TODO: This should not be a switch, it should invoke a hook to get stuff back
    switch (drupal_strtolower($file_extension)) {

      case 'zip':
        if (module_exists('spatial_shapefile')) {
          return spatial_shapefile_get_wkt($uri);
        }
        break;
      case 'shp':
        if ($ogr2ogr_shapefile = ogr2ogr_open($uri)) {
          if ($spatial_features = $ogr2ogr_shapefile->getWkt()) {
            return $spatial_features;
          }
        }
        break;
      case 'kml':
        if (module_exists('spatial_kml')) {
          return spatial_kml_get_wkt($uri);
        }
        break;
      case 'kmz':
        if (module_exists('spatial_kmz')) {
          return spatial_kmz_get_wkt($uri);
        }
        break;
    }

    drupal_set_message(t('Spatial files of this type (!ext) can not currently be processed', array('!ext' => $file_extension)), 'error');
    watchdog('spatial_file', 'Spatial files of this type (!ext) can not currently be processed', array('!ext' => $file_extension),  WATCHDOG_ERROR);
  }
  else {
    watchdog('spatial_file', 'Unable to identify file name extension of spatial file: !uri', array('!uri' => $uri),  WATCHDOG_ERROR);
  }
}


/**
 * Exposed function that other modules may use.
 *
 * Params: $wkt_features as returned by a few different functions
 *         $compress_method how to handle multivalues
 */
function spatial_file_parse_wkt_features($wkt_features, $group_multiple_values = SPATIAL_FILE_GROUPING_COLLECTION) {
  $store_features = array();
  $return_features = array();
  $geometry_pattern = array(
    'Point' => '/^POINT\s*(\(-?\d+\.\d* -?\d+\.\d*( -?\d+\.*\d*)?\))$/i',
    'LineString' => '/^LINESTRING\s*(\(.*\))$/i',
    'Polygon' => '/^POLYGON\s*(\(.*\))$/i',
  );
  $multiple_string = array(
    'Point' => 'MULTIPOINT(%s)',
    'LineString' => 'MULTILINESTRING(%s)',
    'Polygon' => 'MULTIPOLYGON(%s)',
  );

  foreach ($wkt_features as $wkt_feature) {
    if (!isset($wkt_feature['WKT']) || empty($wkt_feature) || $wkt_feature['WKT'] == 'WKT') {
      continue;
    }
    $wkt_feature['WKT'] = _spatial_wkt_reduce($wkt_feature['WKT']);

    switch ($group_multiple_values) {

      case SPATIAL_FILE_GROUPING_COLLECTION:
        $store_features[] = $wkt_feature['WKT'];
        break;

      case SPATIAL_FILE_GROUPING_MULTIPLE:
        $wkt = array('wkt' => $wkt_feature['WKT']);
        $return_features[] = geofield_compute_values($wkt);
        break;

      case SPATIAL_FILE_GROUPING_BYTYPE:

        $geometry = geoPHP::load($wkt_feature['WKT']);
        $feature_type = $geometry->geometryType();

        switch ($feature_type) {

          case 'Point':
          case 'LineString':
          case 'Polygon':
            // These feature types need to be grouped in multiple geometries by type
            $matches = array();
            if (preg_match($geometry_pattern[$feature_type], $wkt_feature['WKT'], $matches)) {
              $store_features[$feature_type][] = $matches[1];
            }

            break;

          case 'MultiPoint':
          case 'MultiLineString':
          case 'MultiPolygon':
            // TODO: These multiple geometries should be broken up so that there is only one for each type
            // For now we just don't want hundreds of fields so we can probably save these as they are
            $wkt = array('wkt' => $wkt_feature['WKT']);
            $return_features[] = geofield_compute_values($wkt);
            break;

          case 'GeometryCollection':
            // Reconstruct the GeometryCollection into a multiple WKT format
            // that this function is already equipped to handle, and feed back into itself
            $constructed_wkt = spatial_geogeometry_break_into_subcomponents($wkt_feature['WKT']);
            if ($parsed_features = spatial_file_parse_wkt_features($constructed_wkt, $group_multiple_values)) {
              $return_features = array_merge($return_features, $parsed_features);
            }
            break;
        }
        break;

    }
  }

  switch ($group_multiple_values) {

    case SPATIAL_FILE_GROUPING_COLLECTION:
      if ($store_features) {
        // If there is more than one value to save, wrap in GeometryCollection
        if (isset($store_features[1])) {
          $wkt = array('wkt' => sprintf('GEOMETRYCOLLECTION(%s)', implode(',', $store_features)));
        }
        else {
          $wkt = array('wkt' => $store_features[0]);
        }
        $return_features[] = geofield_compute_values($wkt);
      }
      break;

    case SPATIAL_FILE_GROUPING_BYTYPE:
      foreach ($store_features as $feature_type => $features) {
        switch ($feature_type) {
          case 'Point':
          case 'LineString':
          case 'Polygon':
            // TODO: If there is just one feature of this type then it shouldn't be multi'ed
            $wkt = array(
              'wkt' => sprintf($multiple_string[$feature_type], implode(',', $features)),
            );
            $return_features[] = geofield_compute_values($wkt);
            break;
        }
      }
      break;
  }

  return $return_features;
}

/**
 * Internal function to break GeometryCollection into subcomponents
 */
function spatial_geogeometry_break_into_subcomponents($wkt){
  $coll_geometry = geoPHP::load($wkt);
  $coll_components = $coll_geometry->getComponents();
  $constructed_wkt = array();
  foreach ($coll_components as $coll_component) {
    if ($coll_component->geometryType() == 'GeometryCollection'){
      $children = spatial_geogeometry_break_into_subcomponents($coll_component->out('wkt'));
      foreach($children as $child){
        $constructed_wkt[] = $child;
      }
    } else {
      $constructed_wkt[] = array('WKT' => $coll_component->out('wkt'));
    }
  }
  return $constructed_wkt;
}


/**
 * Internal function to round WKT values to 7 digits
 */
function _spatial_wkt_reduce($wkt_string, $precision = 7) {
  $current_decimal = $precision - 1;
  $pattern = '/\.(\d{' . $current_decimal . '})([0123456789])[01234]\d*/';
  $wkt_string = preg_replace($pattern, '.${1}$2', $wkt_string);

  while ($current_decimal > 0) {
    $padding = $precision - $current_decimal - 1;
    $nine_string = str_repeat('9', $padding);
    $zero_string = str_repeat('0', $padding);
    for ($i = 0; $i < 9; $i++) {
      $pattern = '/\.(\d{' . $current_decimal . '})' . $i . $nine_string . '[56789]\d*/';
      $replacement = '.${1}' . ($i + 1) . $zero_string;
      $wkt_string = preg_replace($pattern, $replacement, $wkt_string);
    }

    if (!preg_match('/\.\d{' . ($precision + 1) . '}/', $wkt_string)) {
      break;
    }
    $current_decimal--;
  }

  return $wkt_string;
}
